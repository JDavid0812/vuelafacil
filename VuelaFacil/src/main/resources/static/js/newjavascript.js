angular.module('',[])
.controller('filasUsuarios',function($scope,$http){
    
    $scope.idusuarios = 0;
    $scope.Num_identificacion = 0;
    $scope.Nombre = "";
    $scope.Apellido = "";
    $scope.Correo = "";
    $scope.Telefono = 0;
    $scope.Contacto_Emergencia = "";
    
    // accion del boton consultar Desde el ID
    $scope.consultar = function(){
        if($scope.idusuarios === undefined || $scope.idusuarios === null){
            $scope.idusuarios = 0;
        }
        
        $http.get("/cajero/estudiante?cedula="+$scope.idusuarios).then(function(data){
            console.log(data.data);
            $scope.idusuarios = data.data.idusuarios;
            $scope.Num_identificacion = data.data.Num_identificacion;
            $scope.Nombre = data.data.Nombre;
            $scope.Apellido = data.data.Apellido;
            $scope.Correo = data.data.Correo;
            $scope.Telefono = data.data.Telefono;
            $scope.Contacto_Emergencia = data.data.Contacto_Emergencia;
        },function(){
             //error
            $scope.idusuarios = 0;
            $scope.Num_identificacion = 0;
            $scope.Nombre = "";
            $scope.Apellido = "";
            $scope.Correo = "";
            $scope.Telefono = 0;
            $scope.Contacto_Emergencia = "";
            $scope.filas = []; // guarda
        });
        
        $http.get("/cajero/cuentas?cedula="+$scope.idusuarios).then(function(data){
            console.log(data.data);
            $scope.filas = data.data;  
        },function(){
            $scope.filas = [];
        });
    };

//consultar por Vuelos
angular.module('',[])
.controller('filasVuelos',function($scope,$http){
    
    $scope.idvuelos = 0;
    $scope.Codigo_reserva = "";
    $scope.Matricula = "";
    $scope.Aerolinea = "";
    $scope.Hora_salida= 0;
    $scope.Origen = "";
    $scope.Fecha_vuelo = 0;
    
    // accion del boton consultar Desde el ID
    $scope.consultar = function(){
        if($scope.idvuelos === undefined || $scope.idvuelos === null){
            $scope.idvuelos = 0;
        }
        
        $http.get("/cajero/estudiante?cedula="+$scope.idvuelos).then(function(data){
            console.log(data.data);
            $scope.idvuelos = data.data.idvuelos;
            $scope.Codigo_reserva = data.data.Codigo_reserva;
            $scope.Matricula = data.data.Matricula;
            $scope.Aerolinea = data.data.Aerolinea;
            $scope.Hora_salida = data.data.Hora_salida;
            $scope.Origen = data.data.Origen;
            $scope.Fecha_vuelo = data.data.Fecha_vuelo;
        },function(){
             //error
            $scope.idvuelos = 0;
            $scope.Codigo_reserva = "";
            $scope.Matricula = "";
            $scope.Aerolinea = "";
            $scope.Hora_salida= 0;
            $scope.Origen = "";
            $scope.Fecha_vuelo = 0;
            $scope.filas = []; // guarda
        });
        
        $http.get("/cajero/cuentas?cedula="+$scope.idvuelos).then(function(data){
            console.log(data.data);
            $scope.filas = data.data;  
        },function(){
            $scope.filas = [];
        });
    };
