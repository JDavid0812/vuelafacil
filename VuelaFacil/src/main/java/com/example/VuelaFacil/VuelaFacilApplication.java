package com.example.VuelaFacil;

//import com.example.VuelaFacil.modelos.usuarios;

import com.example.logica.Usuarios;
import com.example.logica.Vuelos;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication // Donde se encuentre este arroba, es el Main.
@RestController // Indica que la clase será un API REST.
public class VuelaFacilApplication {

    @Autowired(required = true) // Sirve para interpretar una clase sin necesidad de decirle que cree una instancia nueva. Equivalente a c = new Cuenta();
    Vuelos v; // Instancias - Objeto 
    @Autowired(required = true)
    Usuarios u;

    public static void main(String[] args) {
        SpringApplication.run(VuelaFacilApplication.class, args);
    }

    @GetMapping("/estudiante")
    public String consultarEstudiantePorCedula(@RequestParam(value = "Num_identificacion", defaultValue = "1") int Num_identificacion) throws ClassNotFoundException, SQLException {
        u.setNum_identificacion(Num_identificacion);
        if (u.consultar()) { // True
            String res = new Gson().toJson(u);
            u.setIdusuarios(0); // Si hay más registros SOLO traigase el primero que encuentre
            u.setNombre("");
            u.setApellido("");
            u.setCorreo("");
            u.setTelefono(0);
            u.setContacto_Emergencia("");
            return res;
        } else { // False           
            return new Gson().toJson(u);
        }
    }

    @GetMapping("/cuenta")
    public String consultar(@RequestParam(value = "Num_identificacion", defaultValue = "1") int Num_identificacion) throws ClassNotFoundException, SQLException {
        u.setNum_identificacion(Num_identificacion);
        if (u.consultar()) {
            return new Gson().toJson(u);
        } else {
            return new Gson().toJson(u);
        }
    }

    @GetMapping("/usuarios")
    public String consultarUsuarios(@RequestParam(value = "Num_identificacion", defaultValue = "1") int Num_identificacion) throws ClassNotFoundException, SQLException {
        List<Usuarios> Usuarios = u.consultarTodo();
        if(Usuarios.size() > 1){
            return new Gson().toJson(Usuarios);
            //return "{\"id\":\""+c.getId()+"\",\"numcuenta\":\""+c.getNumero_cuenta()+"\",\"saldo\":\""+c.getSaldo()+"\",\"cvv\":\""+c.getCvv()+"\"}";
        } else {
            return new Gson().toJson(Usuarios);
            //return "{\"id\":\""+cedula+"\",\"numcuenta\":\""+"La cédula no tiene asociada una cuenta."+"\"}";
        }
    }

    // GET - POST - PUT - DELETE
    @PostMapping(path = "/usuario", // le enviamos datos al servidor
            consumes = MediaType.APPLICATION_JSON_VALUE, // recibe json Nombre= Carlos
            produces = MediaType.APPLICATION_JSON_VALUE) // genera un json  => {nombre = Carlos}
    // definición

    // {nombre = Andres, edad = 15}
    public String actualizarUsuario(@RequestBody String usuario) throws ClassNotFoundException, SQLException {
        Usuarios f = new Gson().fromJson(usuario, Usuarios.class); // recibimos el json y lo devolvemos un objeto estudiante
        u.setIdusuarios(f.getIdusuarios()); // Si hay más registros SOLO traigase el primero que encuentre
        u.setNombre(f.getNombre());
        u.setApellido(f.getApellido());
        u.setCorreo(f.getCorreo());
        u.setTelefono(f.getTelefono());
        u.setContacto_Emergencia(f.getContacto_Emergencia());
        u.actualizar();
        return new Gson().toJson(u); // cliente le envia json al servidor. fpr de comunicarse

    }

    @DeleteMapping("/eliminarusuario/{id}")
    public String borrarCuenta(@PathVariable("id") int id) throws SQLException, ClassNotFoundException {
        u.setIdusuarios(id);
        u.borrar();
        return "Los datos del id indicado han sido eliminados";
    }
}