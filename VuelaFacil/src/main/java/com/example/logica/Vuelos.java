package com.example.logica;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component("vuelos")
public class Vuelos {
     @Autowired
    transient JdbcTemplate jdbcTemplate;
    //Atributos 
    private int idvuelos;
    private String Codigo_reserva;
    private int Matricula;
    private String Aerolinea;
    private int Hora_salida;
    private String Origen;
    private int Fecha_vuelo;
    //Constructor 
        public Vuelos(int idvuelos, String Codigo_reserva, int Matricula, String Aerolinea, int Hora_salida, String Origen, int Fecha_vuelo) {
        this.idvuelos = idvuelos;
        this.Codigo_reserva = Codigo_reserva;
        this.Matricula = Matricula;
        this.Aerolinea = Aerolinea;
        this.Hora_salida = Hora_salida;
        this.Origen = Origen;
        this.Fecha_vuelo = Fecha_vuelo;
    }
        public Vuelos() {
    }
     
    //Metodos 

    public int getIdvuelos() {
        return idvuelos;
    }

    public void setIdvuelos(int idvuelos) {
        this.idvuelos = idvuelos;
    }

    public String getCodigo_reserva() {
        return Codigo_reserva;
    }

    public void setCodigo_reserva(String Codigo_reserva) {
        this.Codigo_reserva = Codigo_reserva;
    }

    public int getMatricula() {
        return Matricula;
    }

    public void setMatricula(int Matricula) {
        this.Matricula = Matricula;
    }

    public String getAerolinea() {
        return Aerolinea;
    }

    public void setAerolinea(String Aerolinea) {
        this.Aerolinea = Aerolinea;
    }

    public int getHora_salida() {
        return Hora_salida;
    }

    public void setHora_salida(int Hora_salida) {
        this.Hora_salida = Hora_salida;
    }

    public String getOrigen() {
        return Origen;
    }

    public void setOrigen(String Origen) {
        this.Origen = Origen;
    }

    public int getFecha_vuelo() {
        return Fecha_vuelo;
    }

    public void setFecha_vuelo(int Fecha_vuelo) {
        this.Fecha_vuelo = Fecha_vuelo;
    }

    
   public boolean consultar(){
       String sql = "SELECT idvuelos, Codigo_reserva, Matricula, Aerolinea, Hora_salida, Origen, Fecha_vuelo FROM vuelos WHERE idusuarios =?";
       
       List<Vuelos> vuelos = jdbcTemplate.query(sql, (rs, rowNum)
            ->new Vuelos(
                    rs.getInt("idvuelos"),
                    rs.getString("Codigo_reserva"),
                    rs.getInt("Matricula"),
                    rs.getString("Aerolinea"),
                    rs.getInt("Hora_salida"),
                    rs.getString("Origen"),
                    rs.getInt("Fecha_vuelo")
                    
            ), new Object[]{this.getIdvuelos()});
        if(vuelos != null && !vuelos.isEmpty()){
            this.setIdvuelos(vuelos.get(0).getIdvuelos());
            this.setCodigo_reserva(vuelos.get(0).getCodigo_reserva());
            this.setMatricula(vuelos.get(0).getMatricula());
            this.setAerolinea(vuelos.get(0).getAerolinea());
            this.setHora_salida(vuelos.get(0).getHora_salida());
            this.setOrigen(vuelos.get(0).getOrigen());
            this.setFecha_vuelo(vuelos.get(0).getFecha_vuelo());
            
            return true;
        }
        else {
            return false;
        }    
   
   }  
}