/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.logica;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component("usuarios")
public class Usuarios {

    @Autowired
    transient JdbcTemplate jdbcTemplate;

//Atributos 
    private int idusuarios;
    private int Num_identificacion;
    private String Nombre;
    private String Apellido;
    private String Correo;
    private int Telefono;
    private String Contacto_Emergencia;
    private Vuelos idvuelos; //Llave foranea

    //Constructor
    public Usuarios(int idusuarios, int Num_identificacion, String Nombre, String Apellido, String Correo, int Telefono, String Contacto_Emergencia) {
        super();
        this.idusuarios = idusuarios;
        this.Num_identificacion = Num_identificacion;
        this.Nombre = Nombre;
        this.Apellido = Apellido;
        this.Correo = Correo;
        this.Telefono = Telefono;
        this.Contacto_Emergencia = Contacto_Emergencia;

    }

    public Usuarios() {
    }

    //Metodos
    public int getIdusuarios() {
        return idusuarios;
    }

    public void setIdusuarios(int idusuarios) {
        this.idusuarios = idusuarios;
    }

    public int getNum_identificacion() {
        return Num_identificacion;
    }

    public void setNum_identificacion(int Num_identificacion) {
        this.Num_identificacion = Num_identificacion;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public int getTelefono() {
        return Telefono;
    }

    public void setTelefono(int Telefono) {
        this.Telefono = Telefono;
    }

    public String getContacto_Emergencia() {
        return Contacto_Emergencia;
    }

    public void setContacto_Emergencia(String Contacto_Emergencia) {
        this.Contacto_Emergencia = Contacto_Emergencia;
    }

    public int getIdvuelos() {
        return idvuelos.getIdvuelos();
    }

    public void setIdvuelos(int idvuelos) {
        this.idvuelos.setIdvuelos(idvuelos);
    }

    //CRUD (CREAD)
    public String guardar() throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO usuarios(idusuarios, Num_identificacion, Nombre, Apellido, Correo, Telefono, Contacto_Emergencia, idvuelos) VALUES(?, ?, ?, ?, ?, ?, ?, ?) ";
        return sql;
    }

    //CRUD (READ)
    public boolean consultar() throws ClassNotFoundException, SQLException {
        String sql = "SELEC idusuarios, Num_identificacion, Nombre, Apellido, Correo, Telefono, Contacto_Emergencia, idvuelos FROM usuarios WHERE idvuelos ";
        List<Usuarios> usuario = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Usuarios(
                        rs.getInt("idusuarios"),
                        rs.getInt("Num_identificacion"),
                        rs.getString("Nombre"),
                        rs.getString("Apellido"),
                        rs.getString("Correo"),
                        rs.getInt("Telefono"),
                        rs.getString("Contacto_Emergencia")
                ), new Object[]{this.getIdvuelos()});
        if (usuario != null && !usuario.isEmpty()) {
            this.setIdusuarios(usuario.get(0).getIdusuarios());
            this.setNum_identificacion(usuario.get(0).getNum_identificacion());
            this.setNombre(usuario.get(0).getNombre());
            this.setApellido(usuario.get(0).getApellido());
            this.setCorreo(usuario.get(0).getCorreo());
            this.setTelefono(usuario.get(0).getTelefono());
            this.setContacto_Emergencia(usuario.get(0).getContacto_Emergencia());
            return true;
        } else {
            return false;
        }
    }

    public   List<Usuarios> consultarTodo() throws ClassNotFoundException, SQLException {
        String sql = "SELEC idusuarios, Num_identificacion, Nombre, Apellido, Correo, Telefono, Contacto_Emergencia, idvuelos FROM usuarios WHERE idusuarios ";
        List<Usuarios> usuarios = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Usuarios(
                        rs.getInt("idvuelos"),
                        rs.getInt("Codigo_reserva"),
                        rs.getString("Matricula"),
                        rs.getString("Aerolinea"),
                        rs.getString("Hora_salida"),
                        rs.getInt("Fecha_vuelo"),
                        rs.getString("Origen")
                ), new Object[]{idusuarios});
        return usuarios;
    }

    // CRUD (UPDATE)
    public String actualizar() throws ClassNotFoundException, SQLException {
        String sql = "UPDATE usuario SET Num_identificacion = ?, Nombre = ?, Apellido = ?, Correo = ?, Telefono = ?, Contacto_emergencia= ? WHERE idusuarios= ? ";
        return sql;
    }

    //CRUD (DELETE)
    public boolean borrar() throws ClassNotFoundException, SQLException {
        String sql = "DELETE FROM usuario WHERE idusuarios = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, getIdusuarios());
        ps.execute();
        ps.close();

        return true;

    }

}
